# Paillier
## Environment setup

Run the following command to install python paillier library

```
pip3 install phe  
```

## Play around with Paillier

First of all, please include phe library to your code

```
from phe import paillier as pl
```

To encrypt / decrypt numerical data using paillier, you have to generate public and private keys.
``` 
public_key, private_key = pl.generate_paillier_keypair(n_length=256) 
```


You can encrypt any numerical data using **encrypt()** function in **public_key** object
```
encrypted_24 = public_key.encrypt(24)
```

The encrypted data is an **EncryptedNumber** object.

In paillier, you can perform summation operation between encrypted data and plain-text.
```
encrypted_data_plus_6 = encrypted_24+6
```

The result (**encrypted_data_plus_6**) is still **EncryptedNumber.**

You can see the ciphertext of encrypted data by using **ciphertext()** function.
```
print('Ciphertext of encrypted_24 + 6 is ', encrypted_data_plus_6.ciphertext())
```

You can also perform summation between encrypted data.
```
encrypted_6 = public_key.encrypt(6)
encrypted_plus_encrypted = encrypted_24 + encrypted_6
```

For multiplication and division, you can **only** multiply/divide the encrypted data with plain-text.

```
encrypted_data_mult_2 = encrypted_24*2
encrypted_data_div_2 = encrypted_24/2
```

To decrypt the encrypted data, please use **decrypt()** function in **private_key** object

```
print('Result of encrypted_24 + 6 is ', private_key.decrypt(encrypted_data_plus_6))
```


# Fully Homomorphic Encryption 

Fully Homomorphic Encryption (FHE) is one of encryption scheme that allows us to perform addition, substraction, multiplication, and etc, **over ciphertexts (encrypted data)**. 

Most of the popular FHE libraries, e.g., HELib and Microsoft's SEAL, are all implemented in C++. However, for simplicity, we are going to use a python wrapper of SEAL in our examples.


## Environment setup

Install Pyfhel using command: 
   ``` 
   pip3 install pyfhel 
   ```

## Play around with FHE

First, import Pyfhel, PyCtxt, and PyPtxt to your code.
```
from Pyfhel import Pyfhel, PyCtxt, PyPtxt
```

- PyPtxt is an object for stroing Plain-text data
- PyCtxt is an object for storing ciphertext.
- Pyfhel contains all necessary function for encryption, decryption, and arithmatic operations over PyCtxt.


Next, we have to generate context environment using ***contextGen()*** function. You can consider it as some kind of settings for FHE encryption scheme. 

```
he = Pyfhel()           # Creating empty Pyfhel object
he.contextGen(p=65537, m=1024, flagBatching=True)   #initialize context
```

After you finished initializing the context, you can now generate the key.

```
he.keyGen()             # Key Generation.
```

You can export context, public key, and secret key by using **saveContext()**, **savepublicKey()**, and **savesecretKey()** functions in **Pyfhel** object.

```
he.saveContext('context.txt') #save context to a file
he.savepublicKey('pub.key')  #save public key to a file
he.savesecretKey('secret.key') #save private key to a file
```


To encrypt and decrypt an integer data, please use **encryptInt()** and **decryptInt()** functions in Pyfhel.

```
enc_5 = he.encryptInt(5)
output = he.decryptInt(enc_5)   ## output should be equal to 5.
```


You can save ciphertext of PyCtxt object to a file using **save()** function.
```
enc_5.save('ctxt_enc_5.txt')
```


For float data, please use **encryptFrac()** and **decryptFrac()** functions.

```
enc_2p5 = he.encryptFrac(2.5)
output = he.decryptFrac(enc_2p5)   ## output should be equal to 2.5
```

You can also encrypt an array (packing) to one ciphertext. However, each element in the array should be integer data.

```
enc_array = he.encryptBatch(np.array([1,2,3]))
output = he.decryptBatch(enc_array) #output should be [1 2 3 ............ ]
```

With python, we can use indexing to retrieve the original data from decrypted array.

```
print(output[:3])
```

You can use +, -, and * to perform addition, substraction, and multiply operation between ciphertexts.

```
encA = he.encryptInt(2)
encB = he.encryptInt(3)

enc_A_plus_B = encA + encB
enc_A_minus_B = encA - encB
enc_A_multiply_B = encA * encB
```

**HOWEVER, for the substraction operation, it is better to use negate function in PyFhel instead.**

```
enc_A_minus_B = encA + he.negate(encB)
```

You can only perform opertations on the ciphertexts that are encrypted with the same encoding method. Thus, the following codes will throw exception:

```
encA = he.encryptInt(2)
encB = he.encryptFrac(5.0)

tmp = encA + encB  #this line of codes will throw exception because the encoding methods of encA and encB are different.
```

The most important thing is to keep checking noise level of ciphertext. 
We can check the noise level by using function **noiseLevel()** in **PyFhel** object.

```
encA = he.encryptInt(2)

he.noiseLevel(encA) # this will return noise level of encA
```

**Noise level will be lower every time you perform basic arithmetic operation on ciphertext.
If noise level become 0, it is impossible to get original data back.**



# For your future references ...

In this repository, we used paillier and fhe to implement the following python programs:

1. Linear regression using paillier (linear_regression_with_paillier.py)  
2. Logistic regression using paillier (linear_regression_with_paillier.py)
3. Linear regression using paillier (linear_regression_with_fhe.py)  
4. Logistic regression using paillier (linear_regression_with_fhe.py)

To run these codes, please install **scikit-learn** python package using command:

```
pip3 install scikit-learn
```

**Note:** By installing **scikit-learn**, **numpy** will also be installed automatically.