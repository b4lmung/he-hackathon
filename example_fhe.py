from Pyfhel import Pyfhel, PyPtxt, PyCtxt
import numpy as np

he = Pyfhel()                                       # Creating empty Pyfhel object
he.contextGen(p=65537, m=1024*4, flagBatching=True, fracDigits=20)   # Generating context. 
he.keyGen()                                         # Key Generation.


print('Exporting context & keys to files')
he.saveContext('context.txt') #save context to a file
he.savepublicKey('pub.key')  #save public key to a file
he.savesecretKey('secret.key') #save private key to a file


print('Encryption/Decryption integer')
print('-----------------------------')
enc_5 = he.encryptInt(5)
output = he.decryptInt(enc_5)   ## output should be equal to 5
print('The value of enc_5 is ', output)

print('Saving ciphertext')
print('-----------------------------')
enc_5.save('ctxt_enc_5.txt')


print('\nEncryption/Decryption float')
print('-----------------------------')

enc_2p5 = he.encryptFrac(2.5)
output = he.decryptFrac(enc_2p5)   ## output should be equal to 2.5\
print('The value of enc_2p5 is ', output)

print('\nEncryption/Decryption array ')
print('-----------------------------')

enc_array = he.encryptBatch(np.array([1,2,3]))
output = he.decryptBatch(enc_array)
print('The value of enc_array is ', output[:3]) 


print('\nArithmetic operations on encrypted integer ')
print('-----------------------------')
encA = he.encryptInt(2)
encB = he.encryptInt(3)

enc_A_plus_B = encA + encB
# enc_A_minus_B = encA - encB   # avoid using this
enc_A_minus_B = encA + he.negate(encB)
enc_A_multiply_B = encA * encB

print('encA + encB = ', he.decryptInt(enc_A_plus_B))
print('encA - encB = ', he.decryptInt(enc_A_minus_B))
print('encA x encB = ', he.decryptInt(enc_A_multiply_B))

print('\nArithmetic operations on encrypted float')
print('-----------------------------')

encA = he.encryptFrac(5.5)
encB = he.encryptFrac(5.0)

enc_A_plus_B = encA + encB
# enc_A_minus_B = encA - encB
enc_A_minus_B = encA + he.negate(encB)
enc_A_multiply_B = encA * encB

print('encA + encB = ', he.decryptFrac(enc_A_plus_B))
print('encA - encB = ', he.decryptFrac(enc_A_minus_B))
print('encA x encB = ', he.decryptFrac(enc_A_multiply_B))

print('\nArithmetic operations on encrypted array (element wise operation)')
print('-----------------------------')

encA = he.encryptBatch(np.array([1,2,3]))
encB = he.encryptBatch(np.array([2,4,8]))


enc_A_plus_B = encA + encB
# enc_A_minus_B = encA - encB
enc_A_minus_B = encA + he.negate(encB)
enc_A_multiply_B = encA * encB

print('encA + encB = ', he.decryptBatch(enc_A_plus_B)[:3])
print('encA - encB = ', he.decryptBatch(enc_A_minus_B)[:3])
print('encA x encB = ', he.decryptBatch(enc_A_multiply_B)[:3])


print('\nArithmetic operations between different data type is impossible')
print('-----------------------------')

encA = he.encryptInt(2)
encB = he.encryptFrac(5.0)

try:
    tmp = encA + encB
    print(he.decrypt(tmp))
except:
    print('It is not possible after all')


print('\nNoise budget level')
print('-----------------------------')
encA = he.encryptInt(2)
encB = he.encryptInt(3)

print('Noise level of encA before addition is ', he.noiseLevel(encA))
encA = encA + encB  
print('Noise level of encA after addition is ', he.noiseLevel(encA))
print('Let\'s see whether we can decrypt it or not: encA =', he.decryptInt(encA))
encA = encA * encB
print('Noise level of encA after multiplication is ', he.noiseLevel(encA))
print('Let\'s see whether we can decrypt it or not: encA =', he.decryptInt(encA))

print('.....')
for i in range(20):
    encA = encA * encB

print('Noise level of encA after performing many arithmetic operations is ', he.noiseLevel(encA))
print('The decrypted value of encA when noise level became 0 is ', he.decryptInt(encA))




