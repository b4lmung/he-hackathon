
import numpy as np
import pandas as pd
import math
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from phe import paillier as pl



def simple_sigmoid(input):
    '''
    A simple implementation of sigmoid function.

    Args:
        input: float value.
    
    Returns:
        sigmoid value of input.
    '''

    if input < 0:
        return 1 - 1 / (1 + math.exp(input))
    return 1/(1+math.exp(-input))

def calcualte_sigmoid_and_decrypt_result(private_key: pl.PaillierPrivateKey, pre_prediction_results):
    '''
    (Client side) Function to produce the prediction output.

    Args:
        private_key: PaillierPrivateKey object.
        pre_prediction_results: numpy array obtained from pre_prediction function.

    Returns:
        Two numpy arrays. The first one stores sigmoid value of every predicting sample.
        The second one stores the prediction class label of each predicting sample.

    '''
    output = np.empty(pre_prediction_results.shape, dtype=float)
    output_class = np.empty(pre_prediction_results.shape, dtype=int)

    for i in range(pre_prediction_results.size):
        output[i] =  simple_sigmoid(private_key.decrypt(pre_prediction_results[i])) 
        output_class[i] = 1 if output[i] >= 0.5 else 0
    return output, output_class


def pre_prediction(dataset, model):
    '''
    (Server side) A function to calculate dot product between features and trained_weights.

    Args:
        dataset: numpy array of encrypted feature values of every predicting samples.
        trained_weight: numpy array of weights that have been trained.

    Returns:
        dot product between dataset and trained_weights.
    '''
    coef = model.coef_[0, :]
    return np.dot(dataset, coef)



def preprocess():
    '''
    This function loads dataset (plain-text) and separate them to training & testing dataset.
    '''


    df = pd.read_csv('fraud_balanced.csv')

    #transform nominal data to integer
    for x in df.columns:
        if x in ['credit_usage', 'current_balance', 'num_dependents']:
            continue
        df[x] = LabelEncoder().fit_transform(df[x])

    y = df.iloc[:,-1]
    X = df.iloc[:,0:-1]

    return train_test_split(X,y, test_size=0.2)

def encrypt_dataset(X_test:pd.DataFrame, public_key:pl.PaillierPublicKey):
    '''
    (Client side) This function encrypts testing dataset using Paillier.
    '''
    print(X_test.shape)
    for i in range(X_test.shape[0]):
        for j in range(X_test.shape[1]):
            X_test.iloc[i,j] = public_key.encrypt(int(X_test.iloc[i,j]))
    return X_test

def calculate_accuracy(output_class, y_test):
    '''
    This is an utility function for calculating prediction accuracy.
    '''
    c = 0
    y_test =  y_test.to_list()
    output_test = output_class.tolist()

    print('--- Actual classes --')
    print(y_test)
    print('--- Predicted classes --')
    print(output_test)
    for x in range(len(y_test)):
        if y_test[x] == output_test[x]:
            c+=1

    return 1.0*c/len(y_test)


def main():

    X_train, X_test, y_train, y_test = preprocess()

    public_key, private_key = pl.generate_paillier_keypair(n_length=256)
    model = LogisticRegression()
    model.fit(X_train, y_train)

    enc_x_test = encrypt_dataset(X_test, public_key)

    pre_prediction_results = pre_prediction(enc_x_test, model)
    output, output_class = calcualte_sigmoid_and_decrypt_result(private_key, pre_prediction_results)
    print(f'accuracy: {calculate_accuracy(output_class, y_test)}')


if __name__ == '__main__':
    main()