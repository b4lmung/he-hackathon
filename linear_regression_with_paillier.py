from phe import paillier as pl
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
import random
import numpy as np


def make_datasets_for_linear_regression(total_samples=10, train_ratio=0.8, bias=10):
    '''
    This function generates two sample dataset (training and testing) for linear regression.

    Args:
        total_samples: number of samples to be generated. Default value is 100.
        train_ratio: ratio for training portion. The remaining portion is used for testing. 
            The range of train_ratio should be 0-1.0. Its default value is 0.8.
        bias: bias in linear function. Default value is 10.
         
    Returns:
        Two lists of numeric values. 
        The first one is the training dataset and the second one is the testing dataset.
    '''

    X,Y, actual_coef = make_regression(n_samples=total_samples, n_features=1, bias=bias, coef=True, noise=0.5)
    dataset = [(x[0],y) for x,y in zip(X, Y)]
    print('============== Data generation phase ===================')

    print('Actual weight>', actual_coef)
    print('Actual bias>', bias)

    #shuffle
    random.shuffle(dataset)

    train_size = int(total_samples*train_ratio)
    return dataset[:train_size], dataset[train_size:]


def encrypt_linear_regression_dataset(public_key:pl.PaillierPublicKey, training_dataset:list):
    '''
    This function encrypts a linear regression dataset using Paillier scheme.

    Args:
        public_key: Pyfhel object that has been initialized with public keys and contexts.
        input_data: A list that stores linear regression dataset. 

    Returns:
        a list of encrypted data.
    '''
    enc_input = [(public_key.encrypt(x),y) for x, y in training_dataset]
    return enc_input

    
def training_linear_regression(training_dataset, epoch=100, learning_rate=0.001):
    '''
    Training function for linear regression algorithm.

    Args:
        training_dataset: a python list that stores linear-regression dataset.
        epoch: number of epoch for training. Default value is 10.
        learning_rate: learning rate. Default value is 0.05.

    Returns:
        Trained weight and bias (float values).
    '''

    #initialize weight and bias
    weight = np.random.rand()
    bias = np.random.rand()

    for i in range(epoch):
        error_b = 0
        error_w = 0

        #calculate result
        for x,y in training_dataset:
            prediction = weight*x + bias

            #calculate error
            error = prediction - y

            #for derivatives
            error_b += error
            error_w += error*x

        #calculate gradient of derivatives
        bias = bias - (learning_rate*error_b/len(training_dataset))
        weight = weight - (learning_rate*error_w/len(training_dataset))
   
    return weight, bias

def testing_linear_regression(trained_weight:float, trained_bias:float, encrypted_testing_dataset:list, private_key:pl.PaillierPrivateKey):
    '''
    This function calculates the prediction value from trained weight and bias and print it alongside with the actual result.

    Args:
        trained_weight: PyCtxt of trained_weight.
        trained_bias: PyCtxt of trained_bias.
        encrypted_testing_dataset: a python list that stores encrypted linear-regression dataset.
        private_key: a PaillierPrivateKey for decrypting prediction results.
    '''

    for x,y in encrypted_testing_dataset:
        prediction = trained_weight*x + trained_bias
        print('X=', private_key.decrypt(x), ' Prediction (decrypted)=', private_key.decrypt(prediction), ' Actual=', y)


def linear_regression():
    '''
    This function shows example of training and running linear regression implemented by using Paillier scheme.
    '''
    #number of samples in the dataset
    data_size = 20

    #make sample dataset
    training_dataset, testing_dataset = make_datasets_for_linear_regression(total_samples=data_size, train_ratio=0.8, bias=5)

    #training phase
    epoch = 1000
    learning_rate = 0.005

    #train model using plain text
    print('==============Training with raw (plain text) data===================')
    weight, bias = training_linear_regression(training_dataset, epoch=epoch, learning_rate=learning_rate)
    print('Trained weight >', weight)
    print('Trained bias >', bias)

    print('============== Encrypted testing data ===================')
    #getting keys for paillier
    public_key, private_key = pl.generate_paillier_keypair(n_length=256)

    #encrypt testing data
    encrypted_testing = encrypt_linear_regression_dataset(public_key, testing_dataset)
    
    print('Ciphertext of testing data:')
    #print encrypted data 
    for x,y in encrypted_testing:
        print(x.ciphertext(), '|')

    print('============== Prediction on encrypted data ===================')
    #prediction over encrypted data
    testing_linear_regression(weight, bias, encrypted_testing, private_key)


if __name__ == '__main__':
    linear_regression()