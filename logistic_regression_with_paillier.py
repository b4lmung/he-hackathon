from phe import paillier as pl
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import random
import numpy as np
import pandas as pd
import math


def load_dataset():
    '''
    This function is used for loading and transforming iris dataset from scikit-learn package

    Returns:
        Four numpy arrays. The details of each array are follows:
            1.) First array contains all training samples.
            2.) Second array stores the labels corresponding with the first array.
            3.) Third array contains testing samples.
            4.) Fourth array stores the actual label of each testing sample.

    '''
    iris = load_iris()
    #iris dataset is multiclass with multiple features. 
    #For simplicity, we will choose only first two features & classify whether the sample belongs to class 0 or not.
    X = iris.data[:, :2]
    y = (iris.target != 0) * 1  

    scaler = MinMaxScaler()
    X = scaler.fit_transform(X)

    x_train = np.concatenate((X[:5], X[50:55]))
    y_train = np.concatenate((y[:5], y[50:55]))

    x_test = np.concatenate((X[5:10], X[55:60]))
    y_test = np.concatenate((y[5:10], y[55:60]))

    return x_train, x_test, y_train, y_test


def calculate_accuracy(actual_label, prediction_label):
    '''
    An utility function for calculating the accuracy of the model.

    Args:
        actual_labels: a list that stores actual label of every sample in the testing dataset.
        prediction_labels: a list that stores prediction label outputed from the model.

    Returns: 
        accuracy in string format.
    '''
    correct = 0
    for i, prediction in enumerate(prediction_label):
        if actual_label[i] == prediction:
            correct+=1

    return '{0:.2f}%'.format(100.*correct/prediction_label.size)


def simple_sigmoid(input):
    '''
    A simple implementation of sigmoid function.

    Args:
        input: float value.
    
    Returns:
        sigmoid value of input.
    '''

    if input < 0:
        return 1 - 1 / (1 + math.exp(input))
    return 1/(1+math.exp(-input))


def make_simple_logistic_prediction(encrypted_features, weights):
    '''
    This function predicts the class of a encrypted testing sample.

    Args:
        encrypted_features:  a numpy array that stores encrypted feature values of the input sample.
        weights: a numpy array that stores trained weights.
        
    Returns:
        An EncryptedNumber object that stores a prediction.
    '''
    return simple_sigmoid(np.dot(encrypted_features, weights))

def training_logistic_regression(training_samples, training_labels, epoch=3000, learning_rate=0.01, limited_sum_weights=3):
    '''
    A training function for logistic regression.

    Args:
        training_dataset: a numpy array that stores feature values of every sample in dataset.
        training_labels: a numpy array that stores class labels of every sample in dataset.
        epoch: number of epoch for training. Default value is 10.
        learning_rate: learning rate of logistic regression. Default value is 0.1.
        limited_sum_weights: The summation of the weights should not be higher than this limited_sum_weights threshold. 

    Returns:
        weights
    '''
    
    # initializing weights
    weights = np.zeros(training_samples.shape[1])
    for i in range(epoch):
        cum_grad = np.zeros(training_samples.shape[1])
        tmp_weights = np.array(weights)
        for idx, sample in enumerate(training_samples):
            label = training_labels[idx]
            prediction = make_simple_logistic_prediction(sample, weights)
            grad = np.dot(sample, prediction-label)
            cum_grad += grad
        cum_grad = learning_rate*cum_grad/len(training_samples)
        weights -= cum_grad
        
        #our stopping criteria
        if abs(np.sum(weights)) > limited_sum_weights:
            return tmp_weights
        
    return weights

##################################


def encrypt_logistic_regression_dataset(public_key:pl.PaillierPublicKey, dataset):
    '''
    This functions is used for encrypting dataset using Paillier.

    Args:
        public_key: PaillierPublicKey object.
        dataset: a numpy array that stores feature values of every sample in the dataset.
        
    Returns:
        Numpy array that stores the encrypted dataset.
    '''
    encrypted_dataset = np.empty(dataset.shape, dtype=object)
    for row in range(dataset.shape[0]):
        for col in range(dataset.shape[1]):
            encrypted_dataset[row, col] = public_key.encrypt(dataset[row, col])

    return encrypted_dataset

def pre_prediction(dataset, trained_weights):
    '''
    A function to calculate dot product between features and trained_weights.

    Args:
        dataset: numpy array of encrypted feature values of every predicting samples.
        trained_weight: numpy array of weights that have been trained.

    Returns:
        dot product between dataset and trained_weights.
    '''
    return np.dot(dataset, trained_weights)

def calcualte_sigmoid_and_decrypt_result(private_key: pl.PaillierPrivateKey, pre_prediction_results):
    '''
    Function to produce the prediction output.

    Args:
        private_key: PaillierPrivateKey object.
        pre_prediction_results: numpy array obtained from pre_prediction function.

    Returns:
        Two numpy arrays. The first one stores sigmoid value of every predicting sample.
        The second one stores the prediction class label of each predicting sample.

    '''
    output = np.empty(pre_prediction_results.shape, dtype=float)
    output_class = np.empty(pre_prediction_results.shape, dtype=int)

    for i in range(pre_prediction_results.size):
        output[i] =  simple_sigmoid(private_key.decrypt(pre_prediction_results[i])) 
        output_class[i] = 1 if output[i] >= 0.5 else 0
    return output, output_class



def logistic_regression():
    '''
    This function shows example of training and running logistic regression implemented by using Paillier scheme.
    '''
    #getting keys for paillier
    public_key, private_key = pl.generate_paillier_keypair(n_length=256)
    x_train, x_test, y_train, y_test = load_dataset()

    print('=========== Training logistic regression ===========')
    weights = training_logistic_regression(x_train, y_train, epoch=3, learning_rate=0.1)  #simple training function    
    print('Trained weights > ', weights)

    print('=========== Encrypted test data ===========')
    encrypted_x_test = encrypt_logistic_regression_dataset(public_key, x_test)
 
    for row in range(3):
        for col in range(encrypted_x_test.shape[1]):
            print(encrypted_x_test[row,col].ciphertext(), '|')

    print('=========== Prediction part #1 ===========')
    
    pre_prediction_results = pre_prediction(encrypted_x_test, weights)

    print('=========== Prediction part #2 (client side)===========')
    results, results_classes = calcualte_sigmoid_and_decrypt_result(private_key, pre_prediction_results)
    
    print('Predicted values:\t', results)
    print('Predicted classes:\t', results_classes)
    print('Actual classes:\t', y_test)
    print('Accuracy :\t', calculate_accuracy(y_test, results_classes))


if __name__ == '__main__':
    logistic_regression()