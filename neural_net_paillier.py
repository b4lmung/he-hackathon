import numpy as np
from keras.models import Sequential, model_from_json
from keras.layers.core import Dense, Activation
from keras.utils import to_categorical, np_utils
from keras.optimizers import Adam
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
import keras
import pickle
from phe import paillier as pl



def trainIrisModel():
    '''
    This function train a simple neural network (built by keras) with iris dataset.

    Returns:
        keras model
    '''
    # Use Keras to Train Neural Net for Iris Dataset
    iris = load_iris()
    
    data_X = iris.data
    data_y = iris.target
    data_y = np_utils.to_categorical(data_y)

    X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=0.3, random_state=0)
  
    # Use 2 Dense Layer and ReLU function between.
    model = Sequential()
    model.add(Dense(input_dim=4, output_dim=50, activation='relu'))
    model.add(Dense(input_dim=50, output_dim=3))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])    


    history = model.fit(X_train, y_train,
                        batch_size=32, epochs=80,
                        verbose=0,
                        validation_data=(X_test, y_test),
                        )

    # Save Model to h5 file
    # model.save("iris.h5")

    return model


def encrypt_input(x:list, pk):
    '''
    Encrypt Input (list) using Paillier Encryption.

    Args: 
        x: list of input data
        pk: PaillierPublicKey object for encrypting data
    
    Returns:
        An encrypted x
    '''
    return [pk.encrypt(x[i]) for i in range(len(x))]

def apply_dense_layer(x:list, weights:list, bias:list):
    '''
    This is Dense Layer for Plaintext operation
    Multiply Input with Weight and Add Bias

    Args:
        x: list of input data
        weights:list of trained weights
        bias:list of trained bias

    Returns:
        Innert product between input samples and weights plus bias (wx+b)
    '''
    wx = []
    for i in range(np.shape(weights)[1]):
        inner_product = 0
        for j in range(np.shape(weights)[0]):
            inner_product += x[j]*weights[j][i]
        
        
        wx.append(inner_product + bias[i])

    return wx



def apply_dense_layer_ciphertext(x:list, weights:list, bias:list):
    '''
    Dense Layer for Cipher Text Operation

    Args:
        x: list of encrypted input data
        weights:list of trained weights
        bias:list of trained bias

    Returns:
        Innert product between input samples and weights plus bias (wx+b)
    '''
    result = []
    for i in range(np.shape(weights)[1]):
        inner_product = 0
        for j in range(np.shape(weights)[0]):
            inner_product += x[j]*float(weights[j][i])
        
        
        wx_b = inner_product + float(bias[i])
        result.append(wx_b)

    return result




def apply_relu_layer(x:list):
    '''
    Relu function for Plaintext

    Args: 
        x: list of input data

    Returns:
        list of results of relu function on each element in x
    '''
    result = []
    for i in range(len(x)):
        if x[i] >= 0:
            result.append(x[i])
        else:
            result.append(0)
    return result



def apply_relu_layer_user(x:list, pk, sk):
    '''
    Relu function for Ciphertext,
    As of now, User Recieve Ciphertext and Decyrpt them Temporally,
    Apply Relu and encrypt them back, then need to send them back to Server.
    
    Args: 
        x: list of input data (encrypted)

    Returns:
        list of results of relu function on each element in x (encrypted)
    '''
    x = [sk.decrypt(x[i]) for i in range(len(x))]
    relu_x = apply_relu_layer(x)
    return [pk.encrypt(relu_x[i]) for i in range(len(relu_x))]



def apply_soft_max(x:list):
    '''
    After User Got Result, Decyrpt Result and Apply Softmax

    Args:
        x:  results from neural network before softmax
    
    Return:
        A numpy array of softmax results on x

    '''
    c = np.max(x)
    exp_a = np.exp(x - c)
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a

    return y




def validation(model, isThisPlainText:bool):
    '''
    This function demonstrate on how to use neural network model for predicting plaintext & encrypted data.

    Args: 
        model: keras neural network model
        isThisPlainText: a boolean flag. It is used for choosing whether to predict plaintext data or the encrypted data.

    '''
    # Data for Validation, We use X_test and y_test
    iris = load_iris()
    data_X = iris.data
    data_y = iris.target
    data_y = np_utils.to_categorical(data_y)

    X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=0.3, random_state=0)


    # Load Trained Keras Model and Store Trained Weights and Bias on Memory
    # model = keras.models.load_model("iris.h5")
    layers = model.layers
    
    layer = layers[0]
    weights1 = layer.get_weights()[0]
    bias1 = layer.get_weights()[1]

    layer = layers[1]
    weights2 = layer.get_weights()[0]
    bias2 = layer.get_weights()[1]


    # Put Input into Trained Model for Prediction
    if isThisPlainText:
        # Validation of Neural Net Prediction with Plaintext Input
        total_count = 0
        correct_count = 0
        for i in range(len(X_test)):
            x = X_test[i]

            x = apply_dense_layer(x, weights1, bias1)
            x = apply_relu_layer(x)
            x = apply_dense_layer(x,weights2, bias2)
            x = apply_soft_max(x)

            if np.argmax(x) == np.argmax(y_test[i]):
                correct_count += 1
            else:
                pass
            total_count += 1
            print("Predicted: {0},  Correct: {1}".format(np.argmax(x), np.argmax(y_test[i])))

        print("\n>>>Accuracy is: ", correct_count/total_count)

    else:
        # Validation of Neural Net Prediction with Encrypted Input

        pk, sk = pl.generate_paillier_keypair(n_length=256)
        enc_X_test = []
        for i in range(np.shape(X_test)[0]):
            tmp = []
            for j in range(np.shape(X_test)[1]):
                tmp.append(pk.encrypt(float(X_test[i][j])))
            enc_X_test.append(tmp)

        total_count = 0
        correct_count = 0
        for i in range(len(enc_X_test)):
            
            x = enc_X_test[i]

            x = apply_dense_layer_ciphertext(x, weights1, bias1)
            x = apply_relu_layer_user(x, pk, sk)
            x = apply_dense_layer_ciphertext(x,weights2, bias2)

            x = [sk.decrypt(x[i]) for i in range(len(x))]
            x = apply_soft_max(x)

            if np.argmax(x) == np.argmax(y_test[i]):
                correct_count += 1
            else:
                pass
            total_count += 1

            print("Predicted: {0},  Correct: {1}".format(np.argmax(x), np.argmax(y_test[i])))

        print("\n>>>Accuracy is: ", correct_count/total_count)



if __name__ == "__main__":
    # Validation Function will Execute Prediction with Pre-Trained Neural Net Keras Model.
    # If isThisPlainText=False, Input is Encrypted and Put Through Model.
    # Otherwise, Input is Not Encrypted and Put Through Model for Comparison Purpose.
    model = trainIrisModel()
    print('######## Neural network without encryption ########\n\n')
    validation(model, isThisPlainText=True)


    print('\n\n######## Neural network with encryption ########\n\n')

    validation(model, isThisPlainText=False)
    
    





    
    
    