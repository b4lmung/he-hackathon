import numpy as np
import json
from Pyfhel import Pyfhel, PyPtxt, PyCtxt



# http://www.it.usyd.edu.au/~ikop0282/RecSys2010-final.pdf




def load_and_transform(he: Pyfhel):
    '''
    (User side) 
    This function loads plain text profile & preference of each user to vectors (python list)
    profile and preference vectors of all users are saved into separate python dictionary.
    '''

    with open('sample-profile.json', 'r', encoding='utf8') as reader:
        user_profiles = json.load(reader)
    
    with open('sample-preference.json', 'r', encoding='utf8') as reader:
        user_preferences = json.load(reader)
    
    users = []
    features_header = []
    attributes = []
    for user in user_preferences.keys():
        if user not in users:
            users.append(user)
        for attr in user_preferences[user].keys():
            if attr not in attributes:
                attributes.append(attr)
            for nominal_value in user_preferences[user][attr].keys():
                if f'{attr}#{nominal_value}' not in features_header:
                    features_header.append(f'{attr}#{nominal_value}')
    
    print(f'Number of users is {len(user_preferences.keys())}')
    print(f'Number of features is {len(features_header)}')
    

    print('creating each user\'s preferences')
    prefs = {}
    for user in user_preferences.keys():
        vector = []
        for feature in features_header:
            attr, nominal = feature.split('#')
            vector.append(float(user_preferences[user][attr][nominal]))
        prefs[user] = vector
        print(prefs[user])

    print('*'*30)
    print('creating user profiles')
    profs = {}
    for user in user_profiles.keys():
        vector = [0 for x in features_header]
        for attr in attributes:
            value = user_profiles[user][attr]
            idx = features_header.index(f'{attr}#{value}')
            vector[idx] += 1.0

        profs[user] =  vector
        print(profs[user])
        
    return users, profs, prefs



def encrypt_data(he: Pyfhel, input_profiles_preferences:dict):
    '''
    (User side) This function encrypts user profile/preferences.
    '''
    for u in input_profiles_preferences.keys():
        for i, v in enumerate(input_profiles_preferences[u]):
            input_profiles_preferences[u][i] = he.encryptFrac(v)

def decrypt_data(he: Pyfhel, input_profiles_preferences:dict):
    '''
    (User side) This function decrypts user profile/preferences. 
    '''
    for u in input_profiles_preferences.keys():
        for i, v in enumerate(input_profiles_preferences[u]):
            input_profiles_preferences[u][i] = he.decryptFrac(v)


def calculate_compat(he: Pyfhel, pref_a:list, profile_b:list):
    '''
    (Server side) This function calculate compatability score from user A's preferences and user B's profiles
    Both preferences & profiles are encrypted data.
    '''

    #compatability score
    sum_compat = he.encryptFrac(0)

    #normalize data
    sum_msg = he.encryptFrac(0)

    for x in range(len(pref_a)):
        sum_compat = sum_compat + (pref_a[x] * profile_b[x])
        sum_msg = sum_msg + pref_a[x]

    return (sum_compat, sum_msg)


def decrypt_and_calculate_reciprocal_score(he: Pyfhel, com_xy, com_yx):
    '''
    (Client side) This function decrypts the compatibility score (ctxt) and calculate reciprocal score.
    '''
    com_xy = he.decryptFrac(com_xy[0])/ he.decryptFrac(com_xy[1])
    com_yx = he.decryptFrac(com_yx[0])/ he.decryptFrac(com_yx[1])

    if com_xy != 0:  
        com_xy = 1/ com_xy
    else:
        com_xy = 0
    
    if com_yx != 0:  
        com_yx = 1/ com_yx
    else:
        com_yx = 0
    
    sum_result = com_xy+com_yx
    if  sum_result > 0:
        return 2/sum_result
    
    return 0
    
def main():
    he = Pyfhel()                                       # Creating empty Pyfhel object
    he.contextGen(p=65537, m=1024*4, flagBatching=True, fracDigits=20)   # Generating context. 
    he.keyGen()                                         # Key Generation.

    print('Exporting context & keys to files')
    he.saveContext('context.txt') #save context to a file
    he.savepublicKey('pub.key')  #save public key to a file
    he.savesecretKey('secret.key') #save private key to a file


    users, user_profiles, user_preferences = load_and_transform(he)

    encrypt_data(he, user_profiles)
    encrypt_data(he, user_preferences)

    print('*'*30)
    print('calculate reciprocal score')
    print('*'*30)
    for x in users:
        for y in users:
            if x == y:
                continue

            com_xy = calculate_compat(he, user_preferences[x], user_profiles[y])
            com_yx = calculate_compat(he, user_preferences[y], user_profiles[x])

            score = decrypt_and_calculate_reciprocal_score(he, com_xy, com_yx)
            # resiprocal_score_xy = 2/( (1/decrypt_result(he, com_xy)) + (1/decrypt_result(he, com_yx)))

            print(f'Reciprocal. score of user {x} and {y} : {score}')

if __name__ == '__main__':
    main()

